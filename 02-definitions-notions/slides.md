%title: Nomad
%author: xavki


 ███╗   ██╗ ██████╗ ███╗   ███╗ █████╗ ██████╗ 
████╗  ██║██╔═══██╗████╗ ████║██╔══██╗██╔══██╗
██╔██╗ ██║██║   ██║██╔████╔██║███████║██║  ██║
██║╚██╗██║██║   ██║██║╚██╔╝██║██╔══██║██║  ██║
██║ ╚████║╚██████╔╝██║ ╚═╝ ██║██║  ██║██████╔╝
╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═════╝ 
                                              
# Nomad : Définitions & Notions


-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Client

	* serveur managé par les master

	* principal localisation où les jobs sont déployés

	* peut mettre à disposition différents outils ou drivers

	* ou encore des volumes

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Serveur

	* machines centrales permettant de créer le cluster de manager

	* élection d'un leader (raft)

	* haute disponibilité

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Node Pool

	* groupe de clients (groupe logique) avec les mêmes caractéristiques

	* moyen de gérer l'affinité (spécificité de certaines tâches)

	* exemple : gpu, algos de scheduling différents... 

	* (similaire aux runner gitlab)

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Fédération

	* regroupement de plusieurs régions

	* interaction de n'importe où sur une autre région

<br>

Régions

	* cluster isolé pour les jobs, clients et états

	* latence diff au sein de la région (low) et en dehors

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Plugins

	* Interface de connexion avec nomad pour certaines fonctions

	* CSI / CNI / Devices

<br>

Drivers

	* docker, java, qemu, ecs...

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Job

	* 1 par fichier / 1 fichier

	* description de tâches à réaliser

	* dans un environnement donnée

	* état et statut du job suivi par nomad

	* comprend 1 à plusieurs groupes

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Schedulers

	*	4 type : service / batch / system / system batch

	* comportement par défaut du job

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Group	

	* un ou plusieurs dans un job

	* comprend une à plusieurs tâches

	* lancées sur la même machine (client)

	* affinité, réplication, restarts, service consul/nomad, shutdown

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Network (Group)

	* ports déclarés 

	* fixes ou dynamiques

	* types : host, bridge (similaire à docker)

	* dns...

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Volume (Group)

	* persister la donnée

	* partager des volumes entre le contexte de l'applicatif et le client/host

	* supporte le CSI (Container Storage Interface)

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Task

	* une ou plusieurs actions réalisées dans un groupe

	* templates, artifacts, logs, lifecycle, volumes...


-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Service (Task)

	* interactions réseaux - bloc "network"

	* provider nomad/consul

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>

Template (Task)

	* création de fichier à la volée contenant des variables

	* go template

	* alimenté par les variables d'environnement :
			nomad runtime (NOMAD_JOB_NAME...)

-----------------------------------------------------------------------                                                                             

# Nomad : Définitions & Notions

<br>


ACL (Access Controle List)

	Role : groupe de règles

	Policy : une ou des règles définies à appliquer

	Token : authentification via Bearer Token (accès à un/des policies/roles)
