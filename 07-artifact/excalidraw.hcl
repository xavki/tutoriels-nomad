job "excalidraw" {
  datacenters = ["xavki"]
  type = "service"
  group "tools" {
    count = 1

    network {
      port "http" {
        static = 80
      }
    }

    task "excalidraw" {
      driver = "docker"

      config {
        image = "excalidraw/excalidraw:latest"

        ports = [
          "http"
        ]

      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
      }
      
      service {
        tags = ["ui", "tools"]
				provider = "nomad"
        port = "http"

        check {
          type     = "http"
          name     = "app_health"
          path     = "/"
          interval = "20s"
          timeout  = "10s"

        }
      }
    }
  }
}

