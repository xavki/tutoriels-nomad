%title: Nomad
%author: xavki


 ███╗   ██╗ ██████╗ ███╗   ███╗ █████╗ ██████╗ 
████╗  ██║██╔═══██╗████╗ ████║██╔══██╗██╔══██╗
██╔██╗ ██║██║   ██║██╔████╔██║███████║██║  ██║
██║╚██╗██║██║   ██║██║╚██╔╝██║██╔══██║██║  ██║
██║ ╚████║╚██████╔╝██║ ╚═╝ ██║██║  ██║██████╔╝
╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═════╝ 
                                              
# Nomad : Première Application


-----------------------------------------------------------------------                                                                             

# Nomad : Première Application


nomad node status
nomad job status

nomad job plan excalidraw.hcl
nomad job run excalidraw.hcl
nomad job status excalidraw


nomad job restart excalidraw


