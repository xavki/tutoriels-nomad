%title: Nomad
%author: xavki


 ███╗   ██╗ ██████╗ ███╗   ███╗ █████╗ ██████╗ 
████╗  ██║██╔═══██╗████╗ ████║██╔══██╗██╔══██╗
██╔██╗ ██║██║   ██║██╔████╔██║███████║██║  ██║
██║╚██╗██║██║   ██║██║╚██╔╝██║██╔══██║██║  ██║
██║ ╚████║╚██████╔╝██║ ╚═╝ ██║██║  ██║██████╔╝
╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═════╝ 
                                              


-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Nomad : 

	* orchestrateur d'applications (conteneurs ou non)

	* créé par Hashicorp

	* 661 contributeurs

	* était opensource
		Business Source License (révision du licensing Hashicorp)

	* concurrents : kubernetes, docker swarm, mesos...

	* caractéristique : simplicité et faibles contraintes

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Nomad : 

	* Site Officiel : https://www.nomadproject.io/
	* Github : https://github.com/hashicorp/nomad
	* Tutos Officels : https://developer.hashicorp.com/nomad/tutorials


	=> Version : 1.6.1

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Particularités

	* Langages : Golang & Javascript

	* Configuration : Hashicorp Configuration Language (HCL)

	* Déploiement d'applications : conteneurisées... ou non

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Particularités

	* Interopérable avec :

			* consul (registry de services)

			* et vault, terraform...

			* peut être provider (source) - par ex. Traefik

	* possiblité de scaler

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>


Versions : non mixes et interchangeables

	* community

	* enterprise : multi-region, backups, hot-standby, quotas, audit log...


-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Drivers :

		* docker

		* qemu

		* podman

		* sans (host)

		* ECS

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Kubernetes vs Nomad :

	* courbe d'apprentissage :

		* kubernetes : longue, beaucoup de resources, délicat on-premise

		* nomad : simple, facile on-premise, consul en backend

-----------------------------------------------------------------------                                                       

# Nomad : introduction - c'est quoi ??

<br>

Kubernetes vs Nomad :

	* runtime :

		* kubernetes : forcément des conteneurs, CI/CD parfois à changer,
									 apprentissage d'outils spécifiques (la liste est longue)

		* nomad : adaptation plus facile, pas forcément des conteneurs (mix possible)						

-----------------------------------------------------------------------                                                       

# Nomad : introduction - c'est quoi ??

<br>

Kubernetes vs Nomad :


	* sécurité :

		* kubernetes : très complet, nécessite des connaissances et parfois
									 des outils spécifiques

		* nomad : plus léger, peut être moins complet mais possibilité de revenir
							vers du standard linux

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Kubernetes vs Nomad :

	* évolution :

		* kubernetes : évolutions importantes et continuent, communauté très importante
									 multitudes d'outils (parfois difficile à suivre)

		* nomad : communauté bien plus réduite, peu d'outils annexes
							licence BSL à surveiller

-----------------------------------------------------------------------                                                                             

# Nomad : introduction - c'est quoi ??

<br>

Kubernetes vs Nomad :

	* taille :

		* kubernetes : souvent pour des clusters plus étendues, et beaucoup d'applicatifs

		* nomad : taille plus réduite
