%title: Nomad
%author: xavki


 ███╗   ██╗ ██████╗ ███╗   ███╗ █████╗ ██████╗ 
████╗  ██║██╔═══██╗████╗ ████║██╔══██╗██╔══██╗
██╔██╗ ██║██║   ██║██╔████╔██║███████║██║  ██║
██║╚██╗██║██║   ██║██║╚██╔╝██║██╔══██║██║  ██║
██║ ╚████║╚██████╔╝██║ ╚═╝ ██║██║  ██║██████╔╝
╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═════╝ 
                                              
# Nomad : Installation Cluster sans Consul


-----------------------------------------------------------------------                                                                             

# Nomad : Installation Cluster sans Consul


* master - installation du binaire

```
VERSION=1.6.1
IP=$(hostname -I | awk '{print $2}')
wget -q https://releases.hashicorp.com/nomad/${VERSION}/nomad_${VERSION}_linux_amd64.zip
unzip /home/vagrant/nomad_${VERSION}_linux_amd64.zip
mv /home/vagrant/nomad /usr/local/bin/
chmod +x -R /usr/local/bin/
mkdir -p /var/lib/nomad
chmod -R 700 /var/lib/nomad
mkdir -p /etc/nomad.d
```

* master - ajout de la configuration (attention sur le premier serveur)

```
echo '
datacenter = "xavki"
data_dir = "/var/lib/nomad"
bind_addr = "'${IP}'"
server {
  enabled = true
  bootstrap_expect = 3
  '${JOIN}'
}
telemetry {
  collection_interval = "1s"
  disable_hostname = true
  prometheus_metrics = true
  publish_allocation_metrics = true
  publish_node_metrics = true
}
' > /etc/nomad.d/nomad.hcl
```

* master - mise en place du service systemd

```
echo '[Unit]
Description=Nomad
Documentation=https://www.nomadproject.io/docs
Wants=network-online.target
After=network-online.target
[Service]
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitBurst=3
StartLimitIntervalSec=10
TasksMax=infinity
[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/nomad.service

* master - démarrage du service

```
systemctl enable nomad
systemctl start nomad
```

* worker - isntallation du binaire

```
VERSION=1.6.1
IP=$(hostname -I | awk '{print $2}')
wget -q https://releases.hashicorp.com/nomad/${VERSION}/nomad_${VERSION}_linux_amd64.zip
unzip /home/vagrant/nomad_${VERSION}_linux_amd64.zip
mv /home/vagrant/nomad /usr/local/bin/
chmod +x -R /usr/local/bin/
mkdir -p /var/lib/nomad
chmod -R 700 /var/lib/nomad
mkdir -p /etc/nomad.d
```

* worker - configuration

```
echo '
datacenter = "xavki"
data_dir = "/var/lib/nomad"
bind_addr = "'${IP}'"
client {
  enabled = true
  servers = ["nomad1","nomad2","nomad3"]
  network_interface = "eth1"
  '${JOIN}'
}
' > /etc/nomad.d/nomad.hcl
```

* worker - installation du service systemd

```
echo '[Unit]
Description=Nomad
Documentation=https://www.nomadproject.io/docs
Wants=network-online.target
After=network-online.target
[Service]
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitBurst=3
StartLimitIntervalSec=10
TasksMax=infinity
[Install]
WantedBy=multi-user.target
' > /etc/systemd/system/nomad.service

* worker - start

```
systemctl enable nomad
systemctl start nomad
```
